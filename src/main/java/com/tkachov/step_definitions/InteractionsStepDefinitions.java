package com.tkachov.step_definitions;

import com.tkachov.context.SpringContextAware;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.interactions.Get;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public class InteractionsStepDefinitions implements SpringContextAware {

    @Value("${access.key}")
    private String accessKey;

    @Autowired
    private Actor max;

    @When("I get resource {string}")
    public void i_get_resource(String resource) {
        max.attemptsTo(
                Get.resource(
                        String.format("%s?access_key=%s", resource, accessKey)
                )
        );
    }

    @When("I get resources")
    public void i_get_resources(List<String> resources) {
        max.attemptsTo(
                Get.resource(
                        String.format("%s?access_key=%s", String.join(",", resources), accessKey)
                )
        );
    }

    @When("I get resource {string} with content-type {word}")
    public void i_get_resource(String resource, String contentType) {
        max.attemptsTo(
                Get.resource(
                        String.format("%s?access_key=%s&output=%s", resource, accessKey, contentType)
                )
        );
    }
}
