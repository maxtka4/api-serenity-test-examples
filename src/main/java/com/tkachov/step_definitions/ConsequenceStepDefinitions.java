package com.tkachov.step_definitions;

import com.tkachov.context.SpringContextAware;
import com.tkachov.entities.UserIpMetadata;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;


@Component
public class ConsequenceStepDefinitions implements SpringContextAware {

    @Autowired
    private Actor max;

    @DataTableType
    public UserIpMetadata userIpMetadataEntry(DataTable dataTable) {
        return new UserIpMetadata();
    }

    @Then("I should see response code {int}")
    public void i_should_see_response_code(int code) {
        max.should(
                seeThatResponse("Status code should be " + code,
                        response -> response.statusCode(code)
                ));
    }

    @Then("I should see user ip metadata:")
    public void i_should_see_user_ip_metadata(UserIpMetadata expected) {
        UserIpMetadata actual = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", UserIpMetadata.class);

        assertThat(expected.equals(actual));
    }

    @Then("body with data")
    public void body_with_data() {
        UserIpMetadata actual = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", UserIpMetadata.class);

        assertThat(actual.getRegion_name()).isNotEmpty();
        assertThat(actual.getCountry_name()).isNotEmpty();
        assertThat(actual.getLocation().getCalling_code()).isNotEmpty();
        assertThat(actual.getLocation().getCapital()).isNotEmpty();
    }

    @Then("I should see error info {string}")
    public void i_should_see_error_info(String info) {
        max.should(
                seeThatResponse("Should be correct error ",
                        response -> response
                                .body("error.code", equalTo(303))
                                .body("error.type", equalTo("batch_not_supported_on_plan"))
                                .body("error.info", equalTo(info))
                ));
    }

    @Then("body in xml format")
    public void body_in_xml_format() {
        max.should(
                seeThatResponse("Should be correct error ",
                        response -> response.contentType("application/xml")
                ));
    }

}