package com.tkachov.step_definitions;

import com.tkachov.context.SpringContextAware;
import io.cucumber.java.en.Given;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import org.springframework.beans.factory.annotation.Autowired;

public class AbilityStepDefinitions implements SpringContextAware {

    @Autowired
    private Actor max;

    private EnvironmentVariables environmentVariables;

    @Given("I can call api at url")
    public void i_can_call_api_at_url() {
        max.whoCan(CallAnApi.at(environmentVariables.getProperty("api.url")));
    }
}
