package com.tkachov.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserIpMetadata {

    private String country_name;
    private String region_name;
    private Location location;
}
