Feature: ipstack api

  @001
  Scenario: call base check
    Given I can call api at url
    When I get resource "check"
    Then I should see response code 200
    And body with data

  @002
  Scenario: call by valid ip
    Given I can call api at url
    When I get resource "31.202.101.125"
    Then I should see response code 200
    And I should see user ip metadata:
      | country_name | region_name | calling_code | capital |
      | Ukraine      | Kharkiv     | 380          | Kyiv    |

  @003
  Scenario: batch call by valid ips
    Given I can call api at url
    When I get resources
      | 134.201.250.155 |
      | 72.229.28.185   |
      | 110.174.165.78  |
    Then I should see error info "The Bulk Lookup Endpoint is not supported on the current subscription plan"

  @004
  Scenario: output file type check
    Given I can call api at url
    When I get resource "check" with content-type xml
    Then I should see response code 200
    And body in xml format