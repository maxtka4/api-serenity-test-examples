# Screenplay Pattern with Serenity BDD and Cucumber

This project is a simple demonstration showing how to use screen play pattern with cucumber and serenity framework

## Screenplay implementation

These tests use ability, interactions, consequence step_definitions defined in `src/main/java/com/takchov/step_definitions`.

The overall project structure is shown below:
````
+ context
    Java configuration classes of spring context
+ entities
    api model classes
+ step_definitions
    ability, interactions, consequences principles of screenplay patern
+ features
    test scenarions in gherkin style
````

## Running the project

To run the project you'll need JDK 1.8 and Maven installed.

## Application Under Test

ipstack offers a powerful, real-time IP to geolocation API capable of looking up accurate location data and assessing security threats originating from risky IP addresses

* URL : http://api.ipstack.com/

### Installation and Test Execution

Open the project in any IDE. Run the following command:

```
mvn clean verify
```
To run test cases on gitlab:
- create repository in gitlab and push this project
- gitlab-ci.yml file in project allows to run pipeline automatically after each push to repo  

## Reporting

- The Serenity reports will be generated in the `target/site/serenity` directory. In the serenity directory, open 'index.html' file to view the report.
- Gitlab report will be available under 'Test' tab in pipeline.